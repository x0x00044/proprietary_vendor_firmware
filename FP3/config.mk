FIRMWARE_IMAGES := \
    aboot \
    cmnlib64 \
    cmnlib \
    devcfg \
    dsp \
    keymaster \
    lksecapp \
    mdtp \
    modem \
    rpm \
    sbl1 \
    tz

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
