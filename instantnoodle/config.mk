FIRMWARE_IMAGES := \
    abl \
    aop \
    bluetooth \
    cmnlib \
    cmnlib64 \
    devcfg \
    dsp \
    featenabler \
    hyp \
    imagefv \
    keymaster \
    logo \
    modem \
    multiimgoem \
    qupfw \
    storsec \
    tz \
    uefisecapp \
    xbl \
    xbl_config

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
